#__autora__ : "Ingrid Guaraca"
#__email__ : "ingrid.guaraca@unl.edu.ec"
# One day, going through old books in the attic, a student Bob found an English-Latin dictionary. By that time he spoke
# English fluently, and his dream was to learn Latin. So finding the dictionary was just in time.
# The first line contains a single integer N — the number of English words in the dictionary - followed by N dictionary
# entries. Each entry is contained on a separate line, which contains first the English word, then a hyphen surrounded
# by spaces and then comma-separated list with the translations of this English word in Latin. All the words consist
# only of lowercase English letters. The translations are sorted in lexicographical order. The order of English words
# in the dictionary is also lexicographic.

lat_eng = {}
for i in range(int(input())):
  engp, lats = input().split(' - ')
  for latp in lats.split(', '):
    if latp not in lat_eng:
      lat_eng[latp] = []
    lat_eng[latp].append(engp)
print(len(lat_eng))
for latp in sorted(lat_eng):
  print(latp, '-', ', '.join(sorted(lat_eng[latp])))