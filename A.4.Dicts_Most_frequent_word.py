#__autora__ : "Ingrid Guaraca"
#__email__ : "ingrid.guaraca@unl.edu.ec"
# Given the text: the first line contains the number of lines, then given the lines of words. Print the word in the
# text that occurs most often. If there are many such words, print the one that is less in the alphabetical order.

lines = {}
for i in range(int(input())):
  words = input().split()
  for s in words:
    if s not in lines:
      lines[s] = 0
    lines[s] += 1
max_frequency = max(lines.values())
for s in sorted(lines):
  if lines[s] == max_frequency:
    print(s)
    break