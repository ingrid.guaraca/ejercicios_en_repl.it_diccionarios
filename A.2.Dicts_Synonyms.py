#__autora__ : "Ingrid Guaraca"
#__email__ : "ingrid.guaraca@unl.edu.ec"
# You are given a dictionary consisting of word pairs. Every word is a synonym of the other word in its pair. All the words in the dictionary are different.
# After the dictionary there's one more word given. Find a synonym for it.

s = int(input())
d = {}
for i in range(s):
  one, two= input().split()
  d[one] = two
  d[two] = one
print(d[input()])