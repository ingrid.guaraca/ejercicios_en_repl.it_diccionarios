#__autora__ : "Ingrid Guaraca"
#__email__ : "ingrid.guaraca@unl.edu.ec"
# The first line contains the number of records. After that, each entry contains the name of the candidate and the
# number of votes they got in some state. Count the results of the elections: sum the number of votes for each
# candidate. Print candidates in the alphabetical order.

cant_votes = {}
for i in range(int(input())):
  candi, vot = input().split()
  cant_votes[candi] = cant_votes.get(candi, 0) + int(vot)
for candi, vot in sorted(cant_votes.items()):
  print(candi, vot)