#__autora__ : "Ingrid Guaraca"
#__email__ : "ingrid.guaraca@unl.edu.ec"
# Given a number n, followed by n lines of text, print all words encountered in the text, one per line, with their
# number of occurrences in the text. The words should be sorted in descending order according to their number of
# occurrences, and all words within the same frequency should be printed in lexicographical order.

f = {}
for i in range(int(input())):
  for word in input().split():
    if word not in f:
      f[word] = 0
    f[word] += 1
for j in sorted(set(f.values()), reverse=True):
  for word in sorted([word for word in f if f[word] == j]):
    print(word, j)