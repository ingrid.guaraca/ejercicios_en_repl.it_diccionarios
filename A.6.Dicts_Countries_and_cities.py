#__autora__ : "Ingrid Guaraca"
#__email__ : "ingrid.guaraca@unl.edu.ec"
# Given a list of countries and cities of each country, then given the names of the cities. For each city print the
# country in which it is located.

s = int (input())
Location ={}
for i in range (s):
  a = list(input().split())
  Location[a[0]] = a[1:]
m = int(input())
for t in range (m):
  b = input()
  for country, city in Location.items():
    if b in city:
      print(country)