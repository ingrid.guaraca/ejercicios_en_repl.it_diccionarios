#__autora__ : "Ingrid Guaraca"
#__email__ : "ingrid.guaraca@unl.edu.ec"
# The text is given in a single line. For each word of the text count the number of its occurrences before it.

s = input()
def num(s):
  count = {}
  string = ""
  for word in s.split():
    count[word] = count.get(word, 0) + 1
    string += str(count[word] - 1) + " "
  return string
print(num(s))