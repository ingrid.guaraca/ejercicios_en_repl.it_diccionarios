#__autora__ : "Ingrid Guaraca"
#__email__ : "ingrid.guaraca@unl.edu.ec"
# The virus attacked the filesystem of the supercomputer and broke the control of access rights to the files. For
# file there is a known set of operations which may be applied to it:
# write W,
# read R,
# execute X.

N= int(input())
text = {}
for i in range(N):
  ls = list(input().split())
  text[ls[0]] = ls[1:len(ls)]
M = int(input())
for j in range(M):
  s = list(input().split())
  if 'W' in text[s[1]] and s[0] == 'write':
    print('OK')
  elif 'R' in text[s[1]] and s[0] == 'read':
    print('OK')
  elif 'X' in text[s[1]] and s[0] == 'execute':
    print('OK')
  else:
    print('Access denied')